
### Description

This folder contains a test project to test out actual usage of the plugin.

It's a completely separate gradle project (not a sub-project).

It's not part of the plugin's build, but does depend on you having published
a build to the snapshot repository (uploadArchives task) so that it can depend
on it for testing.

### Running

This build is about testing AWS functionality, so it needs knowledge of an 
AWS account - this is down using AWS credentials (whatever is the account
of the AWS user you specify will be the account that this project uses).

You can set up global AWS credentials if you want and it should just work, 
Graws is designed to use the AWS default provider chain so that crednetials
aren't necessary when running inside an AWS environment.

You can also specify the accessKey/seceretKey in the system properties:
    
    * `grawsTestAccessKey`
    * `grawsTestSecretKey`
    
I usually set these values in ~/.gradle/gradle.properties (i.e. 
`systemProp.grawsTestAccessKey=AKIAXXX`).      