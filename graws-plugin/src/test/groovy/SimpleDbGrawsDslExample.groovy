import static org.graws.beanstalk.BeanstalkOption.*

ext {
}

graws {
  dryRun = true

  dev {  // AWS account

    // these could default to standard gradle tasks?
    taskConfig {
      buildTask = tasks.creatEbZip
      deploymentArchive = tasks.ebCreateZip.ext.deploymentArchive
    }

    bravoWebApp {  // elastic beanstalk application - will be named "bravoWebApp"

      options = [
        (VpcId): "vpc-00000000",
        (Subnets) : "subnet-00000000",
        (ElbSubnets) : "subnet-00000000",
        (SecurityGroups) : "sg-00000000,sg-00000001",
        (AssociatePublicIpAddress) : "false",
        (IamInstanceProfile) : "bravo-profile",
        (InstanceType) : "t2.micro",
        (EnvironmentType) : 'LoadBalanced',
        (MinSize) : '1',
        (MaxSize) : '1',
      ]

      keypair = "bravo-dev-keypair"
      solutionStack = '64bit Amazon Linux 2020.03 v9.9.9 running Docker 1.42.3'
      tags = ["app": "bravo-web"]

      environment = [
        'bravo_web_datasource_url': "jdbc:postgresql://bravo-pg.rds.dev.bravo.org:5432"
      ]

      bravoWebIntEnv {  // EB environment - will be named "bravoWebIntEnv"
        tags = ["env": "int"]

        environment = [
          'bravo_web_datasource_db': "bravo_web_int_db",
          'bravo_web_datasource_user': "bravo_web_int_user",
          'bravo_web_datasource_password': "bravo_web_int_password",
          'bravo_web_server_url': "bravo-int.dev.bravo.org",
        ]
      }

      bravoWebTstEnv {  // EB environment - will be named "bravoWebTstEnv"
        tags = ["env": "tst"]

        environment = [
          'bravo_web_datasource_db': "bravo_web_tst_db",
          'bravo_web_datasource_user': "bravo_web_tst_user",
          'bravo_web_datasource_password': "bravo_web_tst_password",
          'bravo_web_server_url': "bravo-tst.dev.bravo.org",
        ]
      }
    }

  }

}