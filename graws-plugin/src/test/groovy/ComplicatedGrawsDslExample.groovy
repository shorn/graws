import org.graws.beanstalk.VersionCreation

import static org.graws.beanstalk.BeanstalkOption.*

ext {
  devBravoWebDbUrl = "jdbc:postgresql://bravo-pg.rds.dev.bravo.org:5432"
  prdBravoWebDbUrl = "jdbc:postgresql://bravo-pg.rds.prd.bravo.org:5432"
//  project = new org.gradle.api.internal.project.DefaultProject()
}

graws {
  dryRun = true

  dev {  // AWS account
    accessKey = ""
    secretKey = ""

    taskConfig {
      buildTask = tasks.creatEbZip
      deploymentArchive = tasks.ebCreateZip.ext.deploymentArchive
      deploymentVersion = project.version
      versionCreation = VersionCreation.FAIL_IF_EXISTING
      uploadTasks.doFirst {
        assert !project.version.endsWith("-dirty") :
          "don't upload uncommitted files to EB - commit and then re-run"
      }
    }


    options = [
      (VpcId): "vpc-00000000",
      (Subnets) : "subnet-00000000",
      (ElbSubnets) : "subnet-00000000",
      (SecurityGroups) : "sg-00000000,sg-00000001",
    ]

    bravoWeb {  // elastic beanstalk application
      name = "bravo-web-eb-app"
      keypair = "bravo-dev-keypair"
      tags = ["app": "bravo"]
      solutionStack = '64bit Amazon Linux 2015.03 v1.4.3 running Docker 1.6.2'

      options = [
        (AssociatePublicIpAddress) : "false",
        (IamInstanceProfile) : "bravo-profile",
        (InstanceType) : "t2.micro",
        (EnvironmentType) : 'LoadBalanced',
        (MinSize) : '1',
        (MaxSize) : '1',
        (LogPublicationControl): 'true',
        'aws:elasticbeanstalk:application/Application Healthcheck URL': '/login/auth',
        'aws:ec2:vpc/ELBScheme': 'internet-facing',
        'aws:elb:loadbalancer/LoadBalancerHTTPPort': '80',
        'aws:elb:loadbalancer/LoadBalancerHTTPSPort': '443',
        // some way of declaring the account id at account level then reusing it here?
        'aws:elb:loadbalancer/SSLCertificateId':
          'arn:aws:iam::012345678901:server-certificate/wildCardDevBravoCertificateV1',
        'aws:elasticbeanstalk:sns:topics/Notification Protocol': 'email',
        'aws:elasticbeanstalk:sns:topics/Notification Endpoint': 'somebody@bravo.org',
      ]

      // int is reserved java keyword
      'int' {  // elastic beanstalk environment
        name = "bravo-int-env"
        tags = ["env": "int"]
        solutionStack = '64bit Amazon Linux 2020.03 v9.9.9 running Docker 1.42.3'

        environment = [
          'bravo_web_datasource_url': "$devBravoWebDbUrl/bravo_web_int_db",
          'bravo_web_datasource_user': "bravo_web_int_user",
          'bravo_web_datasource_password': "bravo_web_int_password",
          'bravo_web_server_url': "bravo-int.dev.bravo.org",
        ]
      }

      tst {  // elastic beanstalk environment
        name = "bravo-tst-env"
        tags = ["env": "tst"]
        environment = [
          'bravo_web_datasource_url': "$devBravoWebDbUrl/bravo_web_tst_db",
          'bravo_web_datasource_user': "bravo_web_tst_user",
          'bravo_web_datasource_password': "bravo_web_tst_password",
          'bravo_web_server_url': "bravo-tst.dev.bravo.org",
        ]
      }

      uat {  // elastic beanstalk environment
        name = "bravo-uat-env"
        tags = ["env": "uat"]
        environment = [
          'bravo_web_datasource_url': "$devBravoWebDbUrl/bravo_web_uat_db",
          'bravo_web_datasource_user': "bravo_web_uat_user",
          'bravo_web_datasource_password': "bravo_web_uat_password",
          'bravo_web_server_url': "bravo-uat.dev.bravo.org",
        ]
      }
    }
  }

  prd { // AWS account
    options = [
      (VpcId): "vpc-00000000",
      (Subnets) : "subnet-00000000,subnet-00000001",
      (ElbSubnets) : "subnet-00000002, subnet-00000003",
      (SecurityGroups) : "sg-00000000,sg-00000001",
    ]

    bravoWeb { // elastic beanstalk application
      name = "bravo-web-eb-app"
      keypair = "bravo-prd-keypair"
      tags = ["app": "bravo"]
      solutionStack = '64bit Amazon Linux 2015.03 v1.4.3 running Docker 1.6.2'

      options = [
        (AssociatePublicIpAddress) : "false",
        (IamInstanceProfile) : "bravo-profile",
        (InstanceType) : "t2.small",
        (EnvironmentType) : 'LoadBalanced',
        (MinSize) : '2',
        (MaxSize) : '2',
        (LogPublicationControl): 'true',
        'aws:elasticbeanstalk:application/Application Healthcheck URL': '/login/auth',
        'aws:ec2:vpc/ELBScheme': 'internet-facing',
        'aws:autoscaling:asg/Availability Zones': 'any 2',
        'aws:elb:loadbalancer/CrossZone': 'true',
        'aws:elb:loadbalancer/LoadBalancerHTTPPort': '80',
        'aws:elb:loadbalancer/LoadBalancerHTTPSPort': '443',
        // some way of declaring the account id at account level then reusing it here?
        'aws:elb:loadbalancer/SSLCertificateId':
          'arn:aws:iam::109876543210:server-certificate/wildCardPrdBravoCertificateV1',
        'aws:elb:policies/Stickiness Policy': '443',
        'aws:elb:policies/Stickiness Cookie Expiration': '600',
      ]

      prd { // elastic beanstalk environment
        name = "bravo-tst-env"
        tags = ["env": "tst"]
        environment = [
          'bravo_web_datasource_url': "$prdBravoWebDbUrl/bravo_prd_db",
          'bravo_web_datasource_user': "bravo_prd_user",
          'bravo_web_datasource_password': "bravo_web_tst_password",
          'bravo_web_server_url': "bravo.org",
        ]
      }
    }

  }

}