package org.graws

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.graws.docker.DockerExec


class DockerExecTest extends GroovyTestCase {

  /**
   * doesn't test much ATM, more of a dev harness.
   */
  public void testEnvVar(){
    System.setProperty("grawsDocker.key1", "sysVal1")

    Project project = ProjectBuilder.builder().build()
    DockerExec task = (DockerExec) project.task('greeting', type: DockerExec)
    task.quiet = false

    task.environment['key1'] = 'val1'
    task.environment['grawsdocker.key2'] = 'val2'

    task.envVar("key1")
    task.envVar("key2")
    task.envVar("key3")
  }

}
