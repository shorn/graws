package org.graws.docker

class DockerUtilTest extends GroovyTestCase {

  void testParseDocker(){
    String testOutput = """
REPOSITORY                                               TAG                 IMAGE ID            CREATED             VIRTUAL SIZE
localhost/test-svc                                       e22b9ed-dirty       b567033caafd        About an hour ago   872.8 MB
test-docker-registry.dev.testplatform.systems/test-svc   e22b9ed-dirty       7e67efcaa6d8        2 hours ago         872.8 MB
localhost/test-svc                                       58424d5-dirty       a11e2d55996f        19 hours ago        872.8 MB
localhost/test-svc                                       latest              a11e2d55996f        19 hours ago        872.8 MB
localhost/test-svc                                       58424d5             62972982cc5a        19 hours ago        872.8 MB
localhost/test-svc                                       cc7fe4d-dirty       bc0af3e38c94        19 hours ago        872.8 MB
<none>                                                   <none>              d25bea62b979        19 hours ago        872.8 MB
<none>                                                   <none>              d7881ca9a62c        19 hours ago        872.8 MB
<none>                                                   <none>              30a212b7d896        19 hours ago        872.8 MB
localhost/test-svc                                       cc7fe4d             acb6b65a0244        19 hours ago        872.8 MB
localhost/test-svc                                       5f75340-dirty       fa7622f4f026        19 hours ago        872.8 MB
localhost/test-svc                                       7219708-dirty       7a8ed5198d8c        21 hours ago        872.8 MB
<none>                                                   <none>              489e5df1ebcd        21 hours ago        872.8 MB
test-svc                                                 7219708-dirty       eea5b183f2c9        21 hours ago        872.8 MB
localhost/test-docker-registry                           latest              3e989dbd40ab        27 hours ago        548.6 MB
localhost:63042/test-docker-registry                     latest              3e989dbd40ab        27 hours ago        548.6 MB
test-docker-registry                                     latest              7cd1bbeaea02        47 hours ago        548.6 MB
test-svc                                                 latest              4dc5a2f52756        2 days ago          872.8 MB
<none>                                                   <none>              2cac8dde966c        2 days ago          872.8 MB
<none>                                                   <none>              e81eed1156d2        2 days ago          872.8 MB
test-admin                                               latest              04fc5f32b001        2 days ago          878.8 MB
registry                                                 2.0.1               f9684be4155a        8 days ago          548.6 MB
java                                                     openjdk-8u45-jdk    49ebfec495e1        8 days ago          816.4 MB
postgres                                                 9.4.4               f33438ff9aef        9 days ago          265.5 MB
"""
    List<DockerImage> result = DockerUtil.parseDockerImagesResult(testOutput)
    assert result.size() == 24
    assert result[0].repository == "localhost/test-svc"
    assert result[0].tag == "e22b9ed-dirty"
    assert result[0].imageId == "b567033caafd"
  }
}
