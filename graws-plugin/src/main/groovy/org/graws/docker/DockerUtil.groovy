package org.graws.docker

import groovy.transform.ToString
import org.apache.tools.ant.taskdefs.condition.Os

class DockerUtil {
  public static final String ONE_ORE_MORE_SPACES = /( )+/

  /**
   * add environment vars for use by a Exec task when executing a docker
   * command line.
   * <p/>
   * If you're on windows, it will default to setting the env vars for a default
   * boot2docker install.
   */
  static void configureDockerEnvironment(Map environment){
    String userHome = System.properties['user.home']

    String dockerHostDefault = ""
    String dockerCertsDefault = ""
    String dockerTlsVerifyDefault = ""

    if (Os.isFamily(Os.FAMILY_WINDOWS)) {
      // these are boot2docker defaults,
      // not so good anymore now that docker toolbox is the new hotness
      dockerHostDefault = "tcp://192.168.59.103:2376"
      dockerCertsDefault = "$userHome/.boot2docker/certs/boot2docker-vm"
      dockerTlsVerifyDefault = "1"
    }

    String dockerHost = System.getProperty("grawsDockerHost", dockerHostDefault)
    if( dockerHost ) {
      environment['DOCKER_HOST'] = dockerHost
    }
    String dockerCerts = System.getProperty("grawsDockerCerts", dockerCertsDefault)
    if( dockerCerts ) {
      environment['DOCKER_CERT_PATH'] = dockerCerts
    }
    String dockerTlsVerify = System.getProperty("grawsDockerTlsVerify", dockerTlsVerifyDefault)
    if( dockerTlsVerify ) {
      environment['DOCKER_TLS_VERIFY'] = dockerTlsVerify
    }

    // should we default this to a 192.168 pattern somehow?
    String grawsDockerNoProxy = System.getProperty('grawsDockerNoProxy')
    if( grawsDockerNoProxy ){
      if( environment['NO_PROXY'] ){
        environment['NO_PROXY'] = environment['NO_PROXY'] + "," + grawsDockerNoProxy
      }
      else {
        environment['NO_PROXY'] = grawsDockerNoProxy
      }
    }

  }

  /**
   * Parses the result of doing "docker images"
   */
  static List<DockerImage> parseDockerImagesResult(String imagesOuput){
    String[] lines = imagesOuput.trim().split('\n')
    assert lines.size() > 1 : "didn't even get a header line from docker"
    String[] headerLineColumns = lines[0].split(ONE_ORE_MORE_SPACES)
    assert headerLineColumns[0] == "REPOSITORY"
    assert headerLineColumns[1] == "TAG"

    List<DockerImage> result = []
    for( int i=1; i < lines.size(); i++ ){
      assert lines[i]
      String[] iColumns = lines[i].split(ONE_ORE_MORE_SPACES)
      assert iColumns.size() > 2 : "line doesn't look right"

      result << new DockerImage(
        repository: iColumns[0],
        tag: iColumns[1],
        imageId: iColumns[2]
      )
    }

    return result
  }

}

@ToString(includePackage = false)
class DockerImage{
  String repository
  String tag
  String imageId
}
