package org.graws.docker

import groovy.util.logging.Slf4j
import org.apache.tools.ant.taskdefs.condition.Os
import org.gradle.api.tasks.Exec

import static org.apache.tools.ant.taskdefs.condition.Os.FAMILY_WINDOWS

@Slf4j
class DockerExec extends Exec {
  public static final String PROP_PREFIX = "grawsDocker."

  /** getter has default logic */
  String dockerCertPathDefault = null
  /** getter has default logic */
  String dockerHostDefault = null
  /** getter has default logic */
  String dockerTlsVerifyDefault = null

  /** set to false and task will use println, otherwise debug logging */
  boolean quiet = true

  /** set to false if you want the environment to override the system
   * properties.
   * <ul>
   *   <li>if true and both are set, the sysProp value will be used</li>
   *   <li>if false and both are set, the original environment value will
   *   be used</li>
   * </ul>
   */
  boolean sysPropOverridesEnvironment = true

  DockerExec(){
    // if group default is set in exec, then it won't get picked up by the
    // "tasks" task
    group = "docker"
  }

  private void configure() {

    if( !workingDir ){
      debug "setting working dir to project buildDir: ${project.buildDir}"
      workingDir = project.buildDir
    }

    if( !executable ){
      if( project.extensions.extraProperties.has("dockerBinary") ){
        debug "executable set from ext.dockerBinary:" +
          " $project.ext.dockerBinary"
        executable project.ext.dockerBinary
      }
      else {
        debug "setting executable to `binary`, so it needs to be on the path"
        executable 'docker'
      }
    }

    envVar("DOCKER_HOST", getDockerHostDefault())
    envVar("DOCKER_CERT_PATH", getDockerCertPathDefault())
    envVar("DOCKER_TLS_VERIFY", getDockerTlsVerifyDefault())

    envVar("HTTP_PROXY")
    envVar("HTTPS_PROXY")
    envVar("NO_PROXY")
  }

  /**
   * various default configurations, env variables etc. are not set until this
   * executes.  We do it here instead of the Task ctor so that you can do
   * things like set "quiet=false" in a specific gradle task and have only
   * that task output debug info.  If we did config in the ctor, it would
   * be too late and all our debugging would have used the hardcoded value
   * from the ctor isntead of the override from the gradle script.
   * But this means that these defaults won't have been configured on any task
   * until after it executes (so be careful of inter-task dependencies.
   * <p/>
   * BUT having the "group" configured here means that "tasks" task and IDEA
   * don't pick up the correct group, since this doesn't happen until exec time.
   */
  @Override
  protected void exec() {
    configure()

    debug "running ${commandLine} in ${workingDir}"
    // the directory we want to run docker in *must* exist
    project.mkdir(workingDir)

    super.exec()
  }

  String getDockerTlsVerifyDefault() {
    if( dockerTlsVerifyDefault ){
      return dockerTlsVerifyDefault
    }

    if (Os.isFamily(FAMILY_WINDOWS)) {
      // boot2docker default
      return "1"
    }
    else {
      // no idea what a good default would be
      return null
    }
  }


  String getDockerCertPathDefault() {
    if( dockerCertPathDefault ){
      return dockerCertPathDefault
    }

    if (Os.isFamily(FAMILY_WINDOWS)) {
      // boot2docker default
      return "${System.properties['user.home']}" +
        "/.boot2docker/certs/boot2docker-vm"
    }
    else {
      // not sure what a good default cert path is, returning null in case
      // docker itself has a good default
      return null
    }
  }

  String getDockerHostDefault() {
    if( dockerHostDefault ){
      return dockerHostDefault
    }

    if (Os.isFamily(FAMILY_WINDOWS)) {
      // boot2docker default
      return "tcp://192.168.59.103:2376"
    }
    else {
      // assumption being that docker will default to
      // 'unix:///var/run/docker.sock'
      return null
    }
  }

  /**
   * This method is careful to not log env values (because in our SOE
   * environment, there need to be authentication credentials in the proxy
   * string).  It does log the default value if one is used though.
   */
  public void envVar(
    String envVarName,
    String defaultValue = null)
  {
    String sysPropName = makeSysPropName(envVarName)
    String existingEnvValue = environment[envVarName]
    String sysPropValue = System.getProperty(sysPropName)

    if( existingEnvValue && sysPropValue && sysPropOverridesEnvironment ){
      debug "overriding existing environment value named" +
        " `$envVarName` with system property value named" +
        " `$sysPropName` "
      environment[envVarName] = sysPropValue
    }
    else if( existingEnvValue && sysPropValue && !sysPropOverridesEnvironment ){
      debug "keeping existing environment value named" +
        " `$envVarName` even though system property value named" +
        " `$sysPropName` is set"
    }
    else if( existingEnvValue && !sysPropValue ){
      debug "no overriding system property `$sysPropName` defined" +
        " for `$envVarName`, leaving the existing value alone"
    }
    else if( !existingEnvValue && sysPropValue ){
      debug "setting environment variable named `$envVarName` from system" +
        " property named `$sysPropName` "
      environment[envVarName] = sysPropValue
    }
    else if( defaultValue ){
      debug "no env var named `$envVarName` or system property named" +
        " `$sysPropName` is defined, using supplied default value of" +
        " `$defaultValue`"
    }
    else {
      debug "no env var named `$envVarName` or system property named" +
        " `$sysPropName` is defined and there is no default value"
    }
  }

  public String makeSysPropName(String envVarName){
    return "${PROP_PREFIX}${envVarName}"
  }

  public void debug(String msg){
    if( quiet ){
      log.debug(msg)
    }
    else {
      println msg
    }
  }


}
