package org.graws.docker

class DockerBuildExec extends DockerExec {

  /**
   * Extra args that will be put at the beginning of the args just after the
   * build comment itself.
   * i.e. docker build <buildArgs> <proxyArgs> -t, <imageName>, .
   */
  List<String> buildArgs = []

  String imageName

  /**
   * If this is true, the task will try to use a http/https/ftp proxy.
   * Default is true.
   */
  boolean useProxy = true

  /**
   * If this is true, we will use the "--no-cache" option of docker build to
   * avoid using cached docker image layers.
   * Default is false.
   */
  boolean noCache = false

  @Override
  protected void exec() {
    if( args ){
      throw new UnsupportedOperationException(
        "this task is all about creating args for you, just use DockerExec" )
    }

    List<String> imageArgs = ["-t", imageName, "."]
    List<String> proxyArgs = []

    if( useProxy ){
      proxyArgs += makeBuildArgs('HTTP_PROXY')
      proxyArgs += makeBuildArgs('HTTPS_PROXY')
      proxyArgs += makeBuildArgs('http_proxy')
      proxyArgs += makeBuildArgs('https_proxy')
      proxyArgs += makeBuildArgs('ftp_proxy')
    }

    if( noCache ){
      buildArgs += "--no-cache=true"
    }

    args "build"
    args += buildArgs
    args += proxyArgs
    args += imageArgs
    super.exec()
  }

  /**
   * does not log the arg value because it might contain authentication details
   */
  public List<String> makeBuildArgs(String envVarName){
    String sysPropName = makeSysPropName(envVarName)
    String existingEnvValue = environment[envVarName]
    String sysPropValue = System.getProperty(sysPropName)


    if( existingEnvValue || sysPropValue ){
      String buildArg
      if( sysPropOverridesEnvironment ){
        buildArg = "$envVarName=${sysPropValue ?: existingEnvValue}"
      }
      else {
        buildArg = "$envVarName=${existingEnvValue ?: sysPropValue}"
      }
      debug "setting build arg from $envVarName"
      return ["--build-arg", buildArg]
    }
    else {
      debug "no build arg for $envVarName"
      return []
    }

  }

}
