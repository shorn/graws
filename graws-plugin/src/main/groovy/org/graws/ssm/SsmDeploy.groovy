package org.graws.ssm

import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagement
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagementClientBuilder
import com.amazonaws.services.simplesystemsmanagement.model.*
import groovy.util.logging.Slf4j
import org.graws.aws.AwsDeploy

import java.time.Duration

@Slf4j
class SsmDeploy {

  AwsDeploy aws
  String accessKey
  String secretKey

  AWSSimpleSystemsManagement createSsmClient() {
    return aws.customiseClient(
      AWSSimpleSystemsManagementClientBuilder.standard(),
      accessKey, secretKey
    ).build()
  }

  String getParameterValue(String name){
    def client = createSsmClient()
    def request = new GetParameterRequest()
    request.setName(name)
    request.setWithDecryption(true)
    def result = client.getParameter(request)
    return result.parameter.value
  }

  /**
   * @param secure you can change the secure characteristic when overwriting.
   * The downside of this is if you accidentally set a secureString param
   * without setting the secure param, it will default to false and your
   * secure param will be changed to be not secure, oops.
   */
  void putParameterValue(String name, String value, boolean secure=false){
    def client = createSsmClient()
    def request = new PutParameterRequest()
    request.setName(name)
    request.setValue(value)
    request.setOverwrite(true)
    request.setType(secure ? ParameterType.SecureString :  ParameterType.String)
    client.putParameter(request)
  }

  void deleteParameter(String name){
    def client = createSsmClient()

    def request = new DeleteParameterRequest()
    request.setName(name)
    client.deleteParameter(request)
  }

  /**
   * use ("/", true) to get all params.
   */
  Map<String, String> getParametersByPath(
    String path,
    boolean recursive=false,
    int maxParams = 100,
    Duration maxTime = Duration.ofMinutes(2)
  ){
    def client = createSsmClient()

    def request = new GetParametersByPathRequest()
    request.setPath(path)
    request.setRecursive(recursive)
    // on 2018-04-25, AWS call gave error saying 10 is max :/
    request.setMaxResults(10)

    int paramCount = 0
    int chunkCount = 0
    long startTime = System.currentTimeMillis()
    def resultParamPairs = [:]
    def pathResult
    println "listing SSM parameters under $path" +
      " ${recursive?"(recursively)":"(non-recursivley)"}" +
      " - can take a while if there's many"
    while( true ){
      pathResult = client.getParametersByPath(request)
      pathResult.parameters.each {
        resultParamPairs[it.name] = it.value
      }

      paramCount += pathResult.parameters.size()
      chunkCount++

      if( maxParams > 1 && paramCount > maxParams ){
        throw new IllegalArgumentException(
          "Abandoning operation because listed more than maxParams of" +
            " $maxParams, call it with a maxParams of your choosing (or 0)" +
            " if this is really what you want. " + resultParamPairs.values())
      }

      if( pathResult.nextToken ){
        request.setNextToken(pathResult.nextToken)
      }
      else {
        break
      }

      // This code intentionally goes after the break, don't want to fail
      // if it took too long but we're already done anyway
      if(
        maxTime && maxTime.toMillis() > 0 &&
        (System.currentTimeMillis() - startTime) > maxTime.toMillis()
      ){
        throw new IllegalArgumentException("Abandoning operation because" +
          " its taking longer than ${maxTime.toString()}," +
          " processed $chunkCount chunks so far;" +
          " call it with a maxTime of your choosing if that's what you want.")
      }
    }

    return resultParamPairs
  }


}

