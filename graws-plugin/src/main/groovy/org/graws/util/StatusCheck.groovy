package org.graws.util

/**
 * Used by {@link Wait#until(int, int, org.graws.util.StatusCheck)}.
 * <p>
 * STATE is the state of the thing we're waiting for, as returned by the
 * getStatus method
 */
abstract class StatusCheck<STATE> {
  /** Gets the actual current state of the thing we're waiting for*/
  abstract STATE getStatus()

  /** the logic of actually checking if the the thing is ready */
  abstract boolean isReady(STATE status)

  /**
   * Describe  what we're waiting for and the given state, used for
   * progress monitoring.
   */
  abstract describe(STATE status)

  /**
   * The logic of telling if a two STATE's are actually the different, used
   * for progress monitoring.
   * Override this if comparing "toString()" is not sufficient.
   */
  boolean hasChanged(STATE left, STATE right){
    return left.toString() != right.toString()
  }
}
