package org.graws.util

class GrawsUtil {

  static String getSysProp(
    String propName,
    String defaultValue,
    boolean sysPropMandatory = true)
  {
    String propValue = System.getProperty(propName)

    if( sysPropMandatory ){
      assert propValue :
        "define sysprop `${propName}` or use a value of '${defaultValue}'"
    }
    else {
      propValue = defaultValue
    }

    return propValue
  }

}
