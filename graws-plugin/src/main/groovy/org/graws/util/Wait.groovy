package org.graws.util

/**
 * The core waiting logic might be more directly/better expressed using some
 * kind of async/concurrency library, but the point of this method is the
 * progress monitoring.
 * <p>
 * This logic is intended to be used in builds where we're waiting for things
 * to happen and our only way to figure out what's going on/what went wrong
 * will by some CI logs the next morning.
 */
class Wait {

  /**
   * @return true if check.isReady() returns true within the specified time,
   *  false if we wait for waitMinutes and it never comes true.
   */
  public static <STATE> boolean until(
    int waitMinutes = 5,
    int waitIntervalSeconds = 20,
    StatusCheck<STATE> check)
  {
    STATE currentStatus = check.status
    if( check.isReady(currentStatus) ){
      // We were already in the expected state, don't trash the logs with
      // noise from the loop logic.
      println "no need to wait, already ${check.describe(currentStatus)}"
      return true
    }

    println "waiting for ${check.describe(currentStatus)}"

    // this prevents loop logic detecting change on the first iteration
    STATE lastStatus = currentStatus

    long startTime = System.currentTimeMillis()
    long finishTime = startTime + (waitMinutes * 60 * 1000)
    while( !check.isReady(currentStatus) ){
      if( System.currentTimeMillis() > finishTime ){
        // The noise is justified in the situation where the caller is not
        // doing an assert or otherwise using the return value.
        // If an assumption was made that is invalid, this will give them a
        // fighting chance of figuring it out.
        println "$waitMinutes minutes exceeded but never got: " +
          "${check.describe(currentStatus)}"
        return  false
      }

      if( check.hasChanged(currentStatus, lastStatus) ){
        println "status change, but not ${check.describe(currentStatus)}"
        lastStatus = currentStatus
      }
      else {
        // no change in status, but status check is not ready yet
        println "."
      }

      Thread.sleep(waitIntervalSeconds * 1000)
      currentStatus = check.getStatus()
    }

    float timeTakenSeconds = (System.currentTimeMillis() - startTime) / 1000
    println "took $timeTakenSeconds seconds for ${check.describe(currentStatus)}"
    return true
  }

}
