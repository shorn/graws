package org.graws.beanstalk

class GrawsResourceBeanstalkOption {
  String namespace
  String optionName
  String resourceName
  String value

  @Override
  public String toString() {
    return "Option{" +
      "optionName='" + optionName + '\'' +
      ", namespace=" + namespace +
      ", resourceName='" + resourceName + '\'' +
      '}';
  }
}

/**
 * This should be immutable and the with() methods should return a
 * new instance, currently all withXXX() methods return the object they are
 * called on (even withDefaults()).
 */
class EnvironmentDetails {
  /** Required, there's no default that makes sense */
  String solutionStack
  Tier tier = Tier.WebServer

  Map<String, String> tags = [:]
  Map<String, Map<String, String>> namespaceOptions = [:]

  // namespace options are confusing and complicated,
  // screw it, doing it the simple way
  List<GrawsResourceBeanstalkOption> resourceOptions = []


  EnvironmentDetails withDefaults(
    EnvironmentDetails defaultEnv)
  {
    assert defaultEnv
    this.solutionStack = defaultEnv.solutionStack
    this.tags = defaultEnv.tags
    this.namespaceOptions = defaultEnv.cloneNamespaceOptions()
    this.tier = defaultEnv.tier
    return this
  }

  public EnvironmentDetails withSolutionStack(String solutionStack){
    this.solutionStack = solutionStack
    return this
  }

  public EnvironmentDetails withTier(Tier tier){
    this.tier = tier
    return this
  }

  public EnvironmentDetails withTags(Map<String, String> extraTags) {
    tags += extraTags
    return this
  }

  public EnvironmentDetails withElasticbeanstalkEnvironmentOptions(
    Map<String, String> extraOptions) {
    return withOptions(
      BeanstalkOption.Namespace.ElasticbeanstalkEnvironment,
      extraOptions )
  }

  public EnvironmentDetails withEc2VpcOptions(
    Map<String, String> extraOptions) {
    return withOptions(BeanstalkOption.Namespace.Ec2Vpc, extraOptions)
  }

  public EnvironmentDetails withAutoscalingLaunchConfigurationOptions(
    Map<String, String> extraOptions) {
    return withOptions(
      BeanstalkOption.Namespace.AutoscalingLaunchConfiguration, extraOptions)
  }

  public EnvironmentDetails withAutoscalingAsgOptions(
    Map<String, String> extraOptions) {
    return withOptions(BeanstalkOption.Namespace.AutoScalingAsg, extraOptions)
  }

  public EnvironmentDetails withOptions(
    BeanstalkOption.Namespace namespace,
    Map<String, String> options) {
    return withOptions(namespace.namespace, options)
  }

  public EnvironmentDetails withOptions(Map<BeanstalkOption, String> options) {
    options.each { k, v -> withOption(k, v) }
    return this
  }

  public EnvironmentDetails withOption(
    BeanstalkOption option,
    String value)
  {
    assert option
    return withOptions(option.namespace.namespace, [(option.optionName): value])
  }

  public EnvironmentDetails withOption(
    String namespace,
    String option,
    String value)
  {
    assert namespace && option
    return withOptions(namespace, [(option): value])
  }

  /**
   * @param namespace may not be null or empty
   * @param options may be null or empty in which case all options for the
   * given namespace will be deleted.
   */
  public EnvironmentDetails withOptions(
    String namespace, Map<String, String> options) {
    assert namespace

    // remove any pre-existing options that may have existed
    if (options == null || options.isEmpty()) {
      namespaceOptions[namespace] = options ?: [:]
    }

    if (!namespaceOptions.containsKey(namespace)) {
      namespaceOptions[namespace] = [:]
    }

    // this should probably be a deepCopy, cf. cloneNamespaceOptions()
    namespaceOptions[namespace] += options

    return this
  }

  /**
   * Deep copy the map of maps otherwise you'll see all sorts of weirdness
   * if you try to run around with multiple environments copied from each
   * other with withDefaults().
   * <p/>
   * Currently implemented with serialisation because I'm lazy.
   */
  Map<String, Map<String, String>> cloneNamespaceOptions(){
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    ObjectOutputStream oos = new ObjectOutputStream(bos);
    oos.writeObject(namespaceOptions);
    oos.flush();
    oos.close();
    bos.close();
    byte[] byteData = bos.toByteArray();
    ByteArrayInputStream bais = new ByteArrayInputStream(byteData);
    return (Map<String, Map<String, String>>) new ObjectInputStream(bais).readObject();
  }

  EnvironmentDetails withResourceOptions(
    String resourceName,
    String namespace,
    Map<String, String> valueByOptionName)
  {
    valueByOptionName.each { optionValue, optionName ->
      resourceOptions << new GrawsResourceBeanstalkOption(
        resourceName: resourceName,
        namespace: namespace,
        value: optionValue,
        optionName: optionName
      )
    }

    return this
  }
}
