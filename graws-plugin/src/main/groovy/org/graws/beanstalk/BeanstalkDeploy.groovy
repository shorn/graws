package org.graws.beanstalk
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.elasticbeanstalk.AWSElasticBeanstalkClient
import com.amazonaws.services.elasticbeanstalk.model.*
import org.graws.aws.AwsDeploy
import org.graws.beanstalk.check.*
import org.graws.s3.S3Deploy
import org.graws.util.Wait

enum Tier {
  WebServer(new EnvironmentTier().
    withName("WebServer").withVersion("1.0").withType("Standard")),
  Worker(new EnvironmentTier().
  withName("Worker").withVersion("1.0").withType("SQS/HTTP"));

  EnvironmentTier tier

  Tier(EnvironmentTier tier) {
    this.tier = tier
  }
}

enum VersionCreation {
  /** If versionLabel already exists - then fail, don't do anything at all, not
   * even environment creation. */
  FAIL_IF_EXISTING,
  /** if versionLabel already exists - then delete it and re-upload */
  DELETE_EXISTING,
  /** if versionLabel already exists - then skip version creation,
   * just use what's there already, but if it doesn't exist it will upload
   * a new version as described by versionLabel and deploymentArchive. */
  USE_EXISTING
}


class BeanstalkDeploy {

  /** Lazy inited on first use, calling
   * {@link #cleanupCachedEbClient()} is optional */
  BeanstalkClientWrapper eb;

  /** if true, the class won't change any environments, will just print what it
   * would have done (it will still upload version archives though).
   */
  boolean dryRun = false

  boolean reportOptionProblems = false

  AwsDeploy aws
  S3Deploy s3
  String accessKey
  String secretKey

  List<String> findAppVersions(
    BeanstalkClientWrapper eb = getCachedEbWrapper(),
    String appName)
  {

    List<ApplicationDescription> apps = eb.describeApplications(
      new DescribeApplicationsRequest().withApplicationNames(appName))

    // fails during a dry run, what could we do instead?
    if( !apps ){
      throw new IllegalArgumentException("no such appName: '$appName'")
    }

    if( apps.size() > 1 ){
      throw new IllegalArgumentException(
        "too many apps named: '$appName': $apps")
    }

    return apps[0].versions
  }

  void updateEnvironment(
    BeanstalkClientWrapper eb = getCachedEbWrapper(),
    String envName,
    String versionLabel,
    EnvironmentDetails env)
  {
    UpdateEnvironmentRequest updateReq = new UpdateEnvironmentRequest()
    updateReq.versionLabel = versionLabel
    updateReq.environmentName = envName
    ArrayList<ConfigurationOptionSetting> envOptions = []
    List<String> optionProblems = convertOptions(env, envOptions)

    if( reportOptionProblems && optionProblems ){
      println "detected some option problems..."
      optionProblems.each{ println "  $it" }
    }

    updateReq.setOptionSettings(envOptions)

    println "updating environment $envName with version $versionLabel"
    println eb.updateEnvironment(updateReq)
  }

  void createEnvironment(
    BeanstalkClientWrapper eb = getCachedEbWrapper(),
    String appName,
    String envName,
    String versionLabel,
    EnvironmentDetails env)
  {
    assert appName && envName && versionLabel && env
    assert env.solutionStack : " you must specify a solution stack"

    CreateEnvironmentRequest envRequest =
      new CreateEnvironmentRequest(appName, envName)
    envRequest.setVersionLabel(versionLabel);

    envRequest.setSolutionStackName(env.solutionStack);

    assert env.tier
    envRequest.setTier(env.tier.tier)

    envRequest.tags = env.tags.collect{ key, value ->
      new Tag([key: key, value: value])
    }

    ArrayList<ConfigurationOptionSetting> envOptions = []
    List<String> optionProblems = convertOptions(env, envOptions)

    if( reportOptionProblems && optionProblems ){
      println "detected some option problems..."
      optionProblems.each{ println "  $it" }
    }

    envRequest.setOptionSettings(envOptions)
    println "creating environment '$envName'"
    println envRequest
    println "..."

    println "Created " + eb.createEnvironment(envRequest)
  }

  private static List<String> convertOptions(
    EnvironmentDetails env,
    ArrayList<ConfigurationOptionSetting> envOptions)
  {
    List<String> optionProblems = []
    env.namespaceOptions.each { String iNamespace, Map<String, String> iOptionMap ->
      iOptionMap.each { String jKey, String jValue ->
        ConfigurationOptionSetting option =
          new ConfigurationOptionSetting(iNamespace, jKey, jValue)

        envOptions << option
        optionProblems += BeanstalkOption.validateOption(option)
      }
    }

    env.resourceOptions.each{
      envOptions << new ConfigurationOptionSetting(
        it.namespace, it.optionName, it.value).
        withResourceName(it.resourceName)
    }

    return optionProblems
  }

  /**
   * Upload a new version of the application.
   */
  public void uploadCreateAppVersion(
    BeanstalkClientWrapper eb = getCachedEbWrapper(),
    VersionCreation action,
    String appName,
    String versionLabel,
    File deploymentArchive)
  {
    assert appName && versionLabel && action

    ApplicationDescription app = findApplication(eb, appName)
    if( dryRun ){
      app = new ApplicationDescription().withVersions([])
      // intentionally continue through to the actual action
    }

    switch (action) {
      case VersionCreation.USE_EXISTING:
        if (!app.versions.contains(versionLabel)) {
          assert deploymentArchive :
            "no version '$versionLabel' exists but no" +
              " deploymentArchive was supplied to upload"
          S3Location s3 = uploadVersionArchive(eb, deploymentArchive)
          createApplicationVersion(eb, appName, s3, versionLabel)
        }
        break;

      case VersionCreation.DELETE_EXISTING:
        assert deploymentArchive : "no deploymentArchive provided to upload"
        if (app.versions.contains(versionLabel)) {
          println "deleting '$versionLabel' of '$appName' for redeployment"
          eb.deleteApplicationVersion(new DeleteApplicationVersionRequest().
            withApplicationName(appName).
            withVersionLabel(versionLabel))
        }
        S3Location s3 = uploadVersionArchive(eb, deploymentArchive)
        createApplicationVersion(eb, appName, s3, versionLabel)
        break;

      case VersionCreation.FAIL_IF_EXISTING:
        assert deploymentArchive :
          "no deploymentArchive provided to upload"
        if (app.versions.contains(versionLabel)) {
          throw new IllegalStateException("aborting because" +
            " $versionLabel of $appName already exists")
        }
        S3Location s3 = uploadVersionArchive(eb, deploymentArchive)
        createApplicationVersion(eb, appName, s3, versionLabel)
        break;

      default:
        throw new IllegalArgumentException("unknown action ${action} ")
    }
  }

  /**
   * This will create the application if it does not exist.
   * @return may return null (if it's a {@link #dryRun})
   */
  ApplicationDescription createApplicationName(
    BeanstalkClientWrapper eb = getCachedEbWrapper(),
    String applicationName)
  {
    ApplicationDescription application = findApplication(eb, applicationName)

    if (application) {
      println "application '$applicationName' found, skipping creation step"
    } else {
      println "no application with name '$applicationName' found, creating..."
      CreateApplicationRequest appReq =
        new CreateApplicationRequest(applicationName)

      CreateApplicationResult appCreateResult = eb.createApplication(appReq)
      println "Created " + appCreateResult

      application = findApplication(eb, applicationName)
      if( !dryRun ){
        assert application: "created, but then could not find application"
      }
    }
    application
  }

  /**
   * @return will return null if no application found
   */
  public ApplicationDescription findApplication(
    BeanstalkClientWrapper eb = getCachedEbWrapper(),
    String appName)
  {
    DescribeApplicationsRequest req =
      new DescribeApplicationsRequest().withApplicationNames(appName)
    return eb.describeApplications(req).find {
      it.applicationName == appName
    }
  }

  private void createApplicationVersion(
    BeanstalkClientWrapper eb = getCachedEbWrapper(),
    String appName,
    S3Location s3,
    String versionLabel)
  {
//    if( !dryRun ){
//      assert s3 : "no s3 location specified"
//    }
    println "creating $versionLabel of '$appName'..."
    CreateApplicationVersionRequest appVerRequest =
      new CreateApplicationVersionRequest()
    appVerRequest.applicationName = appName
    appVerRequest.versionLabel = versionLabel
    appVerRequest.setAutoCreateApplication(false)
    appVerRequest.setSourceBundle(s3)

    println "Created " + eb.createApplicationVersion(appVerRequest)
  }

  boolean environmentExists(
    BeanstalkClientWrapper eb = getCachedEbWrapper(),
    String envName)
  {
    return eb.describeEnvironments().any{ it.environmentName == envName }
  }

  EnvironmentDescription describeEnvironment(
    BeanstalkClientWrapper eb = getCachedEbWrapper(),
    String envName)
  {
    List<EnvironmentDescription> environments =
      eb.describeEnvironments(envName)
    if( environments.empty ){
      throw new IllegalArgumentException("no environment found for: $envName")
    }

    return environments[0]
  }

  void terminateEnvironment(
    BeanstalkClientWrapper eb = getCachedEbWrapper(),
    String envName)
  {
    eb.terminateEnvironment(
      new TerminateEnvironmentRequest().withEnvironmentName(envName) )
  }

  /**
   * A simpler updateEnvironment exposing the direct EB api types.
   */
  void updateEnvironment(
    BeanstalkClientWrapper eb = getCachedEbWrapper(),
    String envName,
    List<ConfigurationOptionSetting> envOptions = [])
  {
    UpdateEnvironmentRequest updateReq = new UpdateEnvironmentRequest()
    updateReq.environmentName = envName

    updateReq.setOptionSettings(envOptions)

    println "updating environment $envName with options: ${envOptions}"
    println eb.updateEnvironment(updateReq)
  }

  /**
   * returns true if successfully waited for environment to match requirements,
   * returns false if we hit the waitMinutes threshold and the env didn't
   * update to match requirements.
   *
   * @deprecated since 0.2.11 (2016-10-19), use the waitUntil methods or a
   * custom status check instead.
   */
  @Deprecated
  boolean waitForEnvironmentToBeGreen(
    int waitMinutes = 5,
    int waitIntervalSeconds = 60,
    BeanstalkClientWrapper eb = getCachedEbWrapper(),
    String envName,
    String desiredVersion)
  {
    if( dryRun ){
      // probably a bad idea to do the looping construct below in a dry run
      // still might not work for all usecases though (think when the calling
      // logic expects specific state transitions as part of its logic).
      return true
    }

    waitUntilEnv(
      waitMinutes, waitIntervalSeconds, eb,
      "$envName version is `$desiredVersion` and health is Green",
      envName
    ) { EnvironmentDescription e ->
      e.versionLabel == desiredVersion && e.health == "Green"
    }
  }

  /**
   * alias for backwards compatibilty, all new methods are named "wait until"
   * instead of "wait for".
   */
  boolean waitForEnvironmentStatus(
    int waitMinutes = 5,
    int waitIntervalSeconds = 20,
    BeanstalkClientWrapper eb = getCachedEbWrapper(),
    String envName,
    String expectedStatus)
  {
    return waitUntilEnvStatus(
      waitMinutes, waitIntervalSeconds, eb, envName, expectedStatus)
  }

  boolean waitUntilEnvStatus(
    int waitMinutes = 5,
    int waitIntervalSeconds = 20,
    BeanstalkClientWrapper eb = getCachedEbWrapper(),
    String envName,
    String expectedStatus)
  {
    if( dryRun ){
      return true
    }

    return Wait.until(
      waitMinutes,
      waitIntervalSeconds,
      new EnvStatusCheck(envName, expectedStatus, eb) )
  }

  boolean waitUntilEnvHealth(
    int waitMinutes = 5,
    int waitIntervalSeconds = 20,
    BeanstalkClientWrapper eb = getCachedEbWrapper(),
    String envName,
    String expectedHealth)
  {
    if( dryRun ){
      return true
    }

    return Wait.until(
      waitMinutes,
      waitIntervalSeconds,
      new EnvHealthCheck(envName, expectedHealth, eb) )
  }

  /**
   * This one lets you wait until an arbitrary thing happens, usage:
   * <pre>
   * assert ebUtil.waitUntilEnv("integration"){  EnvironmentDescription e ->
   *   e.status == "Ready"  && e.health == "Green"
   * }
   * </pre>
   */
  boolean waitUntilEnv(
    int waitMinutes = 1,
    int waitIntervalSeconds = 5,
    BeanstalkClientWrapper eb = getCachedEbWrapper(),
    String envName,
    String description = "EnvCheck.isReady()",
    Closure<Boolean> readyCheck)
  {
    if( dryRun ){
      return true
    }

    return Wait.until(
      waitMinutes,
      waitIntervalSeconds,
      new EnvCheck(envName, description, eb, readyCheck) )
  }

  /**
   * Forces use of the beanstalk Credentials instead of the s3 Credentials.
   */
  private S3Location uploadVersionArchive(
    BeanstalkClientWrapper eb = getCachedEbWrapper(),
    File deploymentArchive)
  {
    println "uploading to deployment archive to S3: $deploymentArchive.name "
    println "..."
    return eb.putObjectOnS3(deploymentArchive, s3, accessKey, secretKey)
  }

  /**
   * Calls and caches {@link #createNewEbClientForRegion()} if no previous
   * wrapper exists.
   */
  public BeanstalkClientWrapper getCachedEbWrapper(){
    if( eb == null ){
      if( dryRun ){
        eb = new BeanstalkClientWrapper(null, true)
      }
      else {
        eb = new BeanstalkClientWrapper(createNewEbClientForRegion(), false)
      }
    }

    return eb
  }

  /**
   * Calls {@link AWSElasticBeanstalkClient#shutdown()}, which is supposedly an
   * optional call, so I guess calling this method is optional too.
   */
  public void cleanupCachedEbClient(){
    eb.shutdown()
    eb = null
  }

  /**
   * uses credentials passed in
   */
  public AWSElasticBeanstalkClient createNewEbClientForRegion() {
    AWSElasticBeanstalkClient eb = new AWSElasticBeanstalkClient(
        aws.createProviderChain(accessKey, secretKey),
        aws.clientConfiguration )
    eb.setRegion(Region.getRegion(aws.defaultRegion))
    return eb
  }

}

class BeanstalkClientWrapper {
  /** beware: direct usage of this will mean dryRun setting doesn't apply */
  AWSElasticBeanstalkClient eb

  boolean dryRun

  BeanstalkClientWrapper(
    AWSElasticBeanstalkClient eb,
    boolean dryRun = false)
  {
    this.eb = eb
    this.dryRun = dryRun
  }

  List<ApplicationDescription> describeApplications(
    DescribeApplicationsRequest describeApplicationsRequest)
  {
    if( dryRun ){
      println "*** DRY RUN - skipped describeApplications() call "
      return []
    }
    else {
      DescribeApplicationsResult applications =
        eb.describeApplications(describeApplicationsRequest)
      if( applications == null || !applications.applications ){
        return []
      }
      else {
        return applications.applications
      }
    }


  }

  List<EnvironmentDescription> describeEnvironments(
    boolean includeDeleted = false,
    String... envNames)
  {
    if( dryRun ){
      println "*** DRY RUN - skipped describeEnvironments() call "
      return []
    }
    else {
      def req = new DescribeEnvironmentsRequest().
        withEnvironmentNames(envNames).withIncludeDeleted(includeDeleted)
      DescribeEnvironmentsResult environments = eb.describeEnvironments(req)
      return environments.environments
    }
  }

  UpdateEnvironmentResult updateEnvironment(
    UpdateEnvironmentRequest updateEnvironmentRequest)
  {
    if( dryRun ){
      println "*** DRY RUN - skipped updateEnvironment()"
      return new UpdateEnvironmentResult()
    }
    else {
      return eb.updateEnvironment(updateEnvironmentRequest)
    }
  }

  CreateEnvironmentResult createEnvironment(
    CreateEnvironmentRequest createEnvironmentRequest)
  {
    if( dryRun ){
      println "*** DRY RUN - create skipped"
      return new CreateEnvironmentResult()
    }
    else {
      return eb.createEnvironment(createEnvironmentRequest)
    }
  }

  CreateApplicationResult createApplication(CreateApplicationRequest req) {
    if( dryRun ){
      println "*** DRY RUN - skipped createApplciation()"
      return new CreateApplicationResult()
    }
    else {
      return eb.createApplication(req)
    }
  }

  CreateApplicationVersionResult createApplicationVersion(
    CreateApplicationVersionRequest req)
  {
    if( dryRun ){
      println "*** DRY RUN - skipped createApplicationVersion()"
      return new CreateApplicationVersionResult()
    }
    else {
      return eb.createApplicationVersion(req)
    }
  }

  /**
   * dryRun behaviour should be implemented on S3 and dryRun should be pushed
   * up onto the awsDeploy class.
   */
  S3Location putObjectOnS3(
    File deploymentArchive,
    S3Deploy s3Deploy,
    String accessKey,
    String secretKey)
  {
    assert deploymentArchive
    if( dryRun ){
      println "*** DRY RUN - skipped S3 putObject() for : $deploymentArchive"
      return new S3Location()
    }
    else {
      CreateStorageLocationResult location = eb.createStorageLocation();
      String bucket = location.getS3Bucket();
      return s3Deploy.putObject(bucket, deploymentArchive, accessKey, secretKey)
    }
  }

  void deleteApplicationVersion(DeleteApplicationVersionRequest req) {
    if( dryRun ){
      println "*** DRY RUN - skipped deleteApplicationVersion()"
    }
    else {
      eb.deleteApplicationVersion(req)
    }

  }

  TerminateEnvironmentResult terminateEnvironment(
    TerminateEnvironmentRequest req)
  {
    if( dryRun ){
      println "*** DRY RUN - skipped terminateEnvironment()"
    }
    else {
      return eb.terminateEnvironment(req)
    }
  }

  void shutdown() {
    if( dryRun ){
      println "*** DRY RUN - skipped shutdown()"
      return
    }
    eb.shutdown()
  }
}
