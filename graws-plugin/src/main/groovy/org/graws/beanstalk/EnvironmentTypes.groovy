package org.graws.beanstalk

enum EnvironmentTypes {
  SingleInstance, LoadBalanced

  static Boolean isValidValue(String val) {
    BeanstalkOption.isValidEnumValue(EnvironmentTypes, val)
  }
}
