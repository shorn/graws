package org.graws.beanstalk

import com.amazonaws.services.elasticbeanstalk.model.ConfigurationOptionSetting
import org.junit.Test

class BeanstalkDeployTest extends GroovyTestCase{

  @Test
  void testCreateEnvironment(){
    println "test thing"
    BeanstalkDeploy deploy = new BeanstalkDeploy()
    deploy.dryRun = true

    deploy.createEnvironment(
      "x", "y", "z",
      new EnvironmentDetails().
        withTags(['env':'bravo-web-test']).
        withElasticbeanstalkEnvironmentOptions(
          ['EnvironmentType':'LoadBalanced']).
        withAutoscalingAsgOptions(['MinSize':'2']) )
  }

  @Test
  void testValidateOption(){
    assertEquals(
      ["unknown namespace `x`: {Namespace: x,OptionName: y,Value: z}"],
      BeanstalkOption.validateOption(
        new ConfigurationOptionSetting("x", "y", "z") ))

    assertEquals(
      [],
      BeanstalkOption.validateOption(new ConfigurationOptionSetting(
        "aws:elasticbeanstalk:environment",
        "EnvironmentType",
        "SingleInstance" )))
    assertEquals(
      [],
      BeanstalkOption.validateOption(new ConfigurationOptionSetting(
        "aws:ec2:vpc",
        "VPCId",
        "some-vpc" )))
  }

  @Test
  void testIsValidNamespace(){
    assertTrue BeanstalkOption.Namespace.isValidNamespace(
      'aws:elasticbeanstalk:environment')
  }

  @Test
  void testFindOption(){
    assertEquals(0, BeanstalkOption.findAllOptions('Y', 'X').size())

    assertNull BeanstalkOption.findOption('X', 'Y')
    assertNotNull(BeanstalkOption.findOption(
      'EnvironmentType', 'aws:elasticbeanstalk:environment'))
  }
}
