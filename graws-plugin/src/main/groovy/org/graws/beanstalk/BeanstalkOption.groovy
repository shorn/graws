package org.graws.beanstalk

import com.amazonaws.util.StringUtils

import java.util.function.Predicate
import com.amazonaws.services.elasticbeanstalk.model.*

enum BeanstalkOption {
  EnvironmentType(
    'EnvironmentType',
    Namespace.ElasticbeanstalkEnvironment,
    EnvironmentTypes.&isValidValue
  ),

  VpcId('VPCId', Namespace.Ec2Vpc),
  Subnets('Subnets', Namespace.Ec2Vpc),
  ElbSubnets('ELBSubnets', Namespace.Ec2Vpc),
  // seems to be being ignored, maybe coz I've no ELB in front, or mabye
    // I've got the format of the actual value wrong
    AssociatePublicIpAddress('AssociatePublicIpAddress', Namespace.Ec2Vpc),


  Ec2KeyPairName('EC2KeyName', Namespace.AutoscalingLaunchConfiguration),
  InstanceType('InstanceType', Namespace.AutoscalingLaunchConfiguration),
  // this one doesn't seem to work, creating the env is creating a new
  // security group, i think this might be for ELB only
  // or maybe coz the one I was specifying didn't actually have any rules
  SecurityGroups('SecurityGroups', Namespace.AutoscalingLaunchConfiguration),
  IamInstanceProfile(
    'IamInstanceProfile', Namespace.AutoscalingLaunchConfiguration),

  LogPublicationControl('LogPublicationControl', Namespace.EbHostManager),

  MinSize('MinSize', Namespace.AutoScalingAsg),
  MaxSize('MaxSize', Namespace.AutoScalingAsg)


  String optionName
  Namespace namespace
  Predicate<String> isValidValue

  BeanstalkOption(
    String optionName,
    Namespace namespace,
    Predicate<String> isValidValue = { !StringUtils.isNullOrEmpty(it) }) {
    this.optionName = optionName
    this.namespace = namespace
    this.isValidValue = isValidValue
  }

  @Override
  public String toString() {
    return "Option{" +
      "optionName='" + optionName + '\'' +
      ", namespace=" + namespace +
      '}';
  }

  /**
   * @param optionName may not be null or empty
   * @param namespace may be null, in which case searching proceeds on name only
   * @return will return null if no option found
   * @throws IllegalArgumentException if multiple options found
   */
  static BeanstalkOption findOption(String optionName, String namespace) {
    assert optionName
    ArrayList<BeanstalkOption> matching = findAllOptions(optionName, namespace)
    if (matching.size() == 0) {
      return null
    } else if (matching.size() > 1) {
      throw new IllegalArgumentException("found multiple options with" +
        " for ${[optionName, namespace]}, can't choose one")
    } else {
      return matching[0]
    }
  }

  /**
   * @return will not return null, but result may be empty
   */
  public static ArrayList<BeanstalkOption> findAllOptions(
    String optionName, String namespace) {
    ArrayList<BeanstalkOption> matching = values().findAll {
      if (namespace) {
        boolean b = it.optionName == optionName &&
          it.namespace.namespace == namespace
        return b
      } else {
        return it.optionName == optionName
      }
    }
    matching
  }


  static enum Namespace {

    ElasticbeanstalkEnvironment('aws:elasticbeanstalk:environment'),
    Ec2Vpc('aws:ec2:vpc'),
    AutoscalingLaunchConfiguration('aws:autoscaling:launchconfiguration'),
    AutoScalingAsg('aws:autoscaling:asg'),
    AutoScalingScheduledAction('aws:autoscaling:scheduledaction'),
    EbHostManager('aws:elasticbeanstalk:hostmanager')

    String namespace;

    Namespace(String namespace) {
      this.namespace = namespace
    }

    public static boolean isValidNamespace(String value) {
      return values().find { it.namespace == value } != null
    }

    @Override
    public String toString() {
      return namespace
    }
  }

  static <E extends Enum<E>> Boolean isValidEnumValue(
    Class<E> validValues,
    String value) {
    if (!value) {
      return false
    }

    try {
      Enum.valueOf(validValues, value);
      return true;
    } catch (final IllegalArgumentException ignored) {
      return false;
    }
  }

  public static List<String> validateOption(
    ConfigurationOptionSetting option) {
    List<String> optionProblems = []
    if (Namespace.isValidNamespace(option.namespace)) {
      BeanstalkOption optionDefinition = findOption(option.optionName, option.namespace)
      if (optionDefinition) {
        if (!(optionDefinition.isValidValue.test(option.value))) {
          optionProblems << "invalid value `$option.value` for $option"
        }
      } else {
        optionProblems << "unknown option namespace/name $option"
      }

    } else {
      optionProblems << "unknown namespace `${option.namespace}`: $option"
    }

    return optionProblems
  }


}
