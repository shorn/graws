package org.graws.beanstalk.check

import com.amazonaws.services.elasticbeanstalk.model.EnvironmentDescription
import org.graws.beanstalk.BeanstalkClientWrapper
import org.graws.util.StatusCheck

/**
 * This check is used when we want to define our check as a closure, slightly
 * more vervbose in the calling code, but then allows more powerful things, like
 * checking multiple conditions with and/or logic.
 */
class EnvCheck extends StatusCheck<EnvironmentDescription> {
  // config
  String envName
  String description
  Closure<Boolean> isReadyClosure

  // dependencies
  BeanstalkClientWrapper eb

  EnvCheck(
    String envName,
    String description,
    BeanstalkClientWrapper eb,
    Closure<Boolean> isReadyClosure)
  {
    this.envName = envName
    this.description = description
    this.isReadyClosure = isReadyClosure
    this.eb = eb
  }

  String describe(EnvironmentDescription state){
    return "$description: ${state?.toString()}"
  }

  boolean isReady(EnvironmentDescription state){
    return isReadyClosure.call(state)
  }

  EnvironmentDescription getStatus(){
    List<EnvironmentDescription> environments =
      eb.describeEnvironments(envName)
    if( environments.empty ){
      throw new IllegalArgumentException("no environment found for: $envName")
    }

    return environments[0]
  }

}
