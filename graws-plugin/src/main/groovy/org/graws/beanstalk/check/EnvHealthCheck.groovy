package org.graws.beanstalk.check

import com.amazonaws.services.elasticbeanstalk.model.EnvironmentDescription
import org.graws.beanstalk.BeanstalkClientWrapper
import org.graws.util.StatusCheck

class EnvHealthCheck extends StatusCheck<EnvironmentDescription> {
  // config
  String envName
  String expectedHealth

  // dependencies
  BeanstalkClientWrapper eb

  EnvHealthCheck(
    String envName,
    String expectedHealth,
    BeanstalkClientWrapper eb){
    this.envName = envName
    this.expectedHealth = expectedHealth
    this.eb = eb
  }

  String describe(EnvironmentDescription state){
    assert state
    return "$envName is health `$expectedHealth`: ${state.toString()}"
  }

  boolean isReady(EnvironmentDescription state){
    return expectedHealth == state.health
  }

  EnvironmentDescription getStatus(){
    List<EnvironmentDescription> environments =
      eb.describeEnvironments(envName)
    if( environments.empty ){
      throw new IllegalArgumentException("no environment found for: $envName")
    }

    return environments[0]
  }

}
