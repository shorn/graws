package org.graws.beanstalk.check

import com.amazonaws.services.elasticbeanstalk.model.EnvironmentDescription
import org.graws.beanstalk.BeanstalkClientWrapper
import org.graws.util.StatusCheck


class EnvStatusCheck extends StatusCheck<EnvironmentDescription> {
  // config
  String envName
  String expectedStatus

  // dependencies
  BeanstalkClientWrapper eb

  EnvStatusCheck(
    String envName,
    String expectedStatus,
    BeanstalkClientWrapper eb)
  {
    this.envName = envName
    this.expectedStatus = expectedStatus
    this.eb = eb
  }

  String describe(EnvironmentDescription state){
    assert state
    return "$envName is status `$expectedStatus`: ${state.toString()}"
  }

  boolean isReady(EnvironmentDescription state){
    return expectedStatus == state.status
  }

  EnvironmentDescription getStatus(){
    List<EnvironmentDescription> environments =
      eb.describeEnvironments(envName)
    if( environments.empty ){
      throw new IllegalArgumentException("no environment found for: $envName")
    }

    return environments[0]
  }

}

