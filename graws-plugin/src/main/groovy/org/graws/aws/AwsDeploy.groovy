package org.graws.aws

import com.amazonaws.AmazonClientException
import com.amazonaws.ClientConfiguration
import com.amazonaws.auth.*
import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.internal.StaticCredentialsProvider
import com.amazonaws.regions.Regions
import com.amazonaws.util.StringUtils
import com.amazonaws.util.VersionInfoUtils
import groovy.util.logging.Slf4j

@Slf4j
class AwsDeploy implements AWSCredentials {
  static final GrawsCredentialsProvider EMPTY =
    new GrawsCredentialsProvider("", "")

  String accessKey
  String secretKey
  Regions defaultRegion = Regions.AP_SOUTHEAST_2

  boolean envProxyConfig = true

  ClientConfiguration clientConfiguration

  AwsDeploy(){
  }

  // Won't compile without the 'public' keyword.
  // Won't compile if the declaration is line-wrapped anywhere sensible.
  // Groovy is worse than Javascript.
  public <ClientBuilderType extends AwsClientBuilder, TypeToBuild> AwsClientBuilder<ClientBuilderType, TypeToBuild> customiseClient(
    AwsClientBuilder<ClientBuilderType, TypeToBuild> builder,
    String overrideAccessKey, String overrideSecretKey
  ){
    return builder.
      withRegion(defaultRegion).
      withClientConfiguration(clientConfiguration).
      withCredentials( createProviderChain(
        overrideAccessKey, overrideSecretKey  ))
  }

  ClientConfiguration getClientConfiguration(){
    if( clientConfiguration ){
      return clientConfiguration
    }

    this.clientConfiguration = new ClientConfiguration()
    if( envProxyConfig ){
      configureProxyFromEnvironment(this.clientConfiguration)
    }
    else {
      log.info "skipping proxy configuration"
    }

    return clientConfiguration
  }

  /**
   * @param if null, a new clientconfig will be returned
   */
  static ClientConfiguration configureProxyFromEnvironment(
    ClientConfiguration c)
  {
    if( c == null ){
      c = new ClientConfiguration()
    }

    def envProxy = System.getenv("https_proxy") ?: System.getenv("http_proxy")
    if( !envProxy ){
      log.debug "no proxy environment varfound, skipping proxy configuration"
      return c
    }

    URL proxyUrl = new URL(envProxy)
    if( proxyUrl.host ){
      log.info "setting proxy host: ${proxyUrl.host}"
      c.proxyHost = proxyUrl.host
    }
    else {
      log.warn "no host found in the proxy environment variable," +
        " not setting auth info"
      return c
    }

    if( proxyUrl.port ){
      log.info "setting proxy port: ${proxyUrl.port}"
      c.proxyPort = proxyUrl.port
    }
    else {
      log.info "no proxy port set in environment variable, not setting port"
    }

    if( !proxyUrl.userInfo ){
      log.info "proxyUrl exists but doesn't contain userInfo," +
        " not setting auth info"
      return c
    }

    if( !proxyUrl.userInfo.contains(":") ){
      log.info "proxyUrl userInfo exists but doesn't contain a" +
        " colon separator, not setting auth info"
      return c
    }

    def (user, password) = proxyUrl.userInfo.tokenize(':')
    if( !user ){
      log.info "no user found in userinfo, not setting auth info"
    }
    else if( !password ){
      log.info "no password found in userinfo, not setting auth info"
    }
    else {
      log.info "setting proxy authentication info, username `${user}`"
      c.proxyUsername = user
      c.proxyPassword = password
    }

    return c
  }

/**
   * delegates to
   * {@link #createProviderChain(com.amazonaws.auth.AWSCredentialsProvider)}
   */
  AWSCredentialsProviderChain createProviderChain(
    String overrideAccessKey,
    String overrideSecretKey
  ) {
    if( StringUtils.isNullOrEmpty(overrideAccessKey) ){
      return createProviderChain()
    }
    else {
      return createProviderChain(
        new GrawsCredentialsProvider(overrideAccessKey, overrideSecretKey) )
    }
  }

  /**
   * returns a standard provider chain as
   * {@link DefaultAWSCredentialsProviderChain} but with AwsDeploy and override
   * credentials prepended.
   */
  AWSCredentialsProviderChain createProviderChain(
    AWSCredentialsProvider overrideCredentials = EMPTY
  ) {
    return new AWSCredentialsProviderChain(
      overrideCredentials,
      new GrawsCredentialsProvider(this),
      new EnvironmentVariableCredentialsProvider(),
      new SystemPropertiesCredentialsProvider(),
      new ProfileCredentialsProvider(),
      new EC2ContainerCredentialsProviderWrapper(),
      // I think the container creds provider falls back to instance anyway?
      new InstanceProfileCredentialsProvider()
    )

  }

  @Override
  String getAWSAccessKeyId() {
    return accessKey
  }

  @Override
  String getAWSSecretKey() {
    return secretKey
  }

  String getSdkVersion(){
    return VersionInfoUtils.getVersion()
  }


}

class GrawsCredentialsProvider extends StaticCredentialsProvider {
  GrawsCredentialsProvider(AWSCredentials credentials) {
    super(credentials)
  }

  GrawsCredentialsProvider(String accessKey, String secretKey) {
    super(new BasicAWSCredentials(accessKey, secretKey))
  }

  @Override
  AWSCredentials getCredentials() {
    def creds = super.getCredentials()
    if( StringUtils.isNullOrEmpty(creds.AWSAccessKeyId) ){
      throw new AmazonClientException("GrawsCredentials contain no accessKey")
    }
    return creds
  }

  @Override
  String toString() {
    return "${super.toString()} - " +
      "accessKey: ${super.credentials.AWSAccessKeyId}"
  }
}