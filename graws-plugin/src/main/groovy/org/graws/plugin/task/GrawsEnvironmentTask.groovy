package org.graws.plugin.task
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.tasks.TaskAction
import org.graws.beanstalk.EnvironmentDetails
import org.graws.beanstalk.VersionCreation
import org.graws.plugin.GrawsExtension

class GrawsEnvironmentTask extends DefaultTask {

  String env
  String version
  EnvironmentDetails envDetails

  boolean checkDirty = true
  String dirtyMessage =
    "please don't upload uncommitted files to EB - commit and then re-run"

  GrawsExtension getGraws(){
    return project.extensions.graws
  }

  Project getRootProject(){
    return super.project.rootProject
  }

  void configureVersion(){
    if( !version ){
      version = rootProject.version
      println "version not set, using root project version: `${version}`"
    }
  }

  void checkDirty(){
    if( checkDirty ){
      assert !version.endsWith("-dirty") : dirtyMessage
    }
  }

}

/**
 * Will create the application if it does not exist,
 * see {@link UploadNewEnvironmentVersionTask#createApplication}
 */
class UploadNewEnvironmentVersionTask extends GrawsEnvironmentTask {

  String app
  File archive

  /**
   * Action to take if a version already exists
   */
  VersionCreation ifVersionExists = VersionCreation.USE_EXISTING

  /**
   * Create the application if it does not exist, defaults to true.
   */
  boolean createApplication = true

  /**
   * Create the environment if it doesn't exist, defaults to true.
   */
  boolean createEnvironment = true

  @TaskAction
  void uploadEnvironmentVersion() {
    configureVersion()
    assert env && version && envDetails
    checkDirty()

    println "deploying version: ${version} from: $archive.absolutePath"
    if( createApplication ){
      graws.eb.createApplicationName(app)
    }

    graws.eb.uploadCreateAppVersion(ifVersionExists, app, version, archive)

    if( graws.eb.environmentExists(env) ){
      graws.eb.updateEnvironment(env, version, envDetails)
    }
    else{
      if( createEnvironment ){
        graws.eb.createEnvironment(app, env, version, envDetails)
      }
      else {
        throw new IllegalStateException(
          "environment doesn't exist: `$env`, task not configured to create")
      }
    }
  }
}



class PromoteEnvironmentVersionTask extends GrawsEnvironmentTask {

  @TaskAction
  void promoteEnvironmentVersion() {
    configureVersion()
    assert env && version && envDetails
    checkDirty()

    println "promoting version: ${version} to environment ${env}"
    graws.eb.updateEnvironment(env, version, envDetails)
  }
}



