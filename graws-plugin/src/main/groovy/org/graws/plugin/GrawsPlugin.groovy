package org.graws.plugin

import com.amazonaws.util.VersionInfoUtils
import groovy.sql.Sql
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.graws.aws.AwsDeploy
import org.graws.beanstalk.BeanstalkDeploy
import org.graws.ec2.Ec2Deploy
import org.graws.ecr.EcrDeploy
import org.graws.iam.IamDeploy
import org.graws.rds.RdsDeploy
import org.graws.rds.postgres.PgDeploy
import org.graws.s3.S3Deploy
import org.graws.ssm.SsmDeploy

class GrawsExtension {
  Project project

  AwsDeploy aws = new AwsDeploy()
  S3Deploy s3 = new S3Deploy(aws: aws)
  BeanstalkDeploy eb = new BeanstalkDeploy(aws: aws, s3: s3)
  RdsDeploy rds = new RdsDeploy(awsDeploy: aws)
  IamDeploy iam = new IamDeploy(aws: aws)
  PgDeploy pg = new PgDeploy(rds: rds)
  Ec2Deploy ec2 = new Ec2Deploy(aws: aws)
  EcrDeploy ecr = new EcrDeploy(aws: aws)
  SsmDeploy ssm = new SsmDeploy(aws: aws)

  GrawsExtension(Project project) {
    this.project = project
  }

  /**
   * allows configuring all the deploy objects without having to write
   * a bunch of "def aws(Closure)" methods
   */
  void methodMissing(String name, def args) {
    if(
      this.hasProperty(name) &&
        args.length == 1 &&
        args[0] instanceof Closure
    ){
      project.configure(this.getProperty(name), (Closure) args[0])
    }
    else {
      throw new MissingMethodException(name, AwsDeploy, args)
    }

  }
}

class GrawsPlugin implements Plugin<Project> {

  @Override
  void apply(Project project) {
    project.task('grawsVersion') {
      description = "print the version of Graws plugin"
      group = "graws"
      doLast {
        // depends on impl version being set correctly in the Manifest file,
        // see jar task in plugin gradle file
        println "Graws " +
          getClass().getPackage().getImplementationVersion()
        println "AWS SDK " +
          VersionInfoUtils.getVersion()
      }
    }

    project.extensions.create('graws', GrawsExtension, project)

    project.configurations.create('grawsJdbcDriver')

    project.task('grawsInitJdbcDriver'){
      description = "adds the dependencies needed for the JDBC driver to the" +
        " classpath, dependencies should be defined in in the grawsJdbcDriver" +
        " configuration"
      group = "graws"
      doLast {
        // be careful when playing with this, if you're using gradlew, the class
        // only needs to be loaded once; don't remove this and think "oh, it
        // still works without it, guess it wasn't needed".
        project.configurations.grawsJdbcDriver.files.each {
          Sql.classLoader.addURL(it.toURI().toURL())
        }
      }
    }

  }


}
