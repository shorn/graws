package org.graws.s3

import com.amazonaws.services.elasticbeanstalk.model.S3Location
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.*
import groovy.util.logging.Slf4j
import org.gradle.api.Nullable
import org.graws.aws.AwsDeploy

import java.util.function.Function
/**
 * This class does not do any caching of S3Client objects, each call creates
 * a new S3Client, which is eligible for GC after method completion (I couldn't
 * see any close/dispose functionality on {@link AmazonS3Client}.
 * If there is client dispose functionality, this class should be modified to
 * use it.
 */
@Slf4j
class S3Deploy {

  AwsDeploy aws
  String accessKey
  String secretKey

  /**
   * delegates to
   * {@link #putObject(java.lang.String, java.lang.String, java.io.File, java.lang.String, java.lang.String)}
   * using the filename as the key.
   */
  S3Location putObject(
    String bucket,
    File file,
    String overrideAccessKey,
    String overrideSecretKey,
    boolean grantOwnerControl=false)
  {
    putObject(
      bucket,
      file.name,
      file,
      overrideAccessKey,
      overrideSecretKey,
      grantOwnerControl )
  }

  S3Location putObject(
    String bucket,
    String s3Key,
    File file,
    boolean grantOwnerControl=false)
  {
    putObject(bucket, s3Key, file, null, null, grantOwnerControl)
  }

  AmazonS3 createS3Client(){
    return AmazonS3ClientBuilder.standard().
      withClientConfiguration(aws.clientConfiguration).
      withCredentials(aws.createProviderChain(accessKey, secretKey)).
      build()

  }

  /**
   * Uses the provided credentials instead of any that might be set on
   * the S3Deploy class (if null is passed in, will use AwsDeploy credentials
   * if they exist).
   */
  S3Location putObject(
    String bucket,
    String s3Key,
    File file,
    String overrideAccessKey,
    String overrideSecretKey,
    boolean grantOwnerControl=false)
  {
    PutObjectRequest putRequest = new PutObjectRequest(bucket, s3Key, file)
    if( grantOwnerControl ){
      putRequest = putRequest.withCannedAcl(
        CannedAccessControlList.BucketOwnerFullControl)
    }

    AmazonS3Client s3Client = new AmazonS3Client(
      aws.createProviderChain(overrideAccessKey, overrideSecretKey),
      aws.clientConfiguration )

    PutObjectResult putRes = s3Client.putObject(putRequest)
    // should move this out to caller somehow?
    log.info "uploaded " + putRes

    return new S3Location(bucket, s3Key)
  }

  /**
   * Uses credentials set on this class or on the AwsDeploy.
   *
   * @param grantOwnerControl will apply
   * {@link CannedAccessControlList#BucketOwnerFullControl}, defaults to false
   * for API compatibility (old calls that don't specify the toggle will work
   * exactly how they did before).
   */
  S3Location putObject(
    String bucket,
    File file,
    boolean grantOwnerControl=false)
  {
    PutObjectRequest putRequest = new PutObjectRequest(bucket, file.name, file)
    if( grantOwnerControl ){
      putRequest = putRequest.withCannedAcl(
        CannedAccessControlList.BucketOwnerFullControl)
    }

    AmazonS3 s3Client = createS3Client()

    PutObjectResult putRes = s3Client.putObject(putRequest)
    // should move this out to caller somehow?
    log.info "uploaded " + putRes

    return new S3Location(bucket, file.name)
  }

  S3Object getObject(
    String bucket,
    String key)
  {
    GetObjectRequest getRequest = new GetObjectRequest(bucket, key)

    AmazonS3 s3Client = createS3Client()
    return s3Client.getObject(getRequest)
  }

  File getFile(
    String bucket,
    String key,
    File resultDir,
    String resultFileName)
  {
    GetObjectRequest getRequest = new GetObjectRequest(bucket, key)

    AmazonS3 s3Client = createS3Client()

    S3Object s3Object = s3Client.getObject(getRequest)
    try {
      File resultFile = new File(resultDir, resultFileName)

      FileOutputStream resultOutStream = new FileOutputStream(resultFile)

      S3ObjectInputStream objectInputStream = s3Object.objectContent

      try {
        resultOutStream << objectInputStream
      }
      finally {
        objectInputStream.close()
        resultOutStream.close()
      }

      return resultFile

    }
    finally {
      s3Object.close()
    }
  }

  ObjectMetadata getObjectMetaData(String bucketName, String key) {
    AmazonS3 s3Client = aws.createS3Client(accessKey, secretKey)

    def request = new GetObjectMetadataRequest(bucketName, key)
    return s3Client.getObjectMetadata(request)
  }

  /**
   * credentials used must have list permission for this to work.
   *
   * @param key is really a prefix, see {@link ListObjectsRequest}
   */
  boolean isObjectInBucket(String bucketName, String key) {
//    AmazonS3 s3Client = createS3Client()

    def request = new ListObjectsRequest().
      withBucketName(bucketName).
      withPrefix(key)

    return findObject(request){ it.key == key }
  }

  /**
   * Util method to assist in dealing with paged list results.
   *
   * @param handler will be called to handle the results for each page, page
   * iteration will stop when there are no more pages or hanlder retunrs true.
   */
  void processAllListObjects(
    ListObjectsRequest request,
    Function<ObjectListing, ListScan> handler)
  {
    AmazonS3 s3Client = createS3Client()

    ObjectListing listing = s3Client.listObjects(request)
    ListScan continueListing = handler.apply(listing)

    while( continueListing == ListScan.CONTINUE && listing.isTruncated() ){
      listing = s3Client.listNextBatchOfObjects(listing)
      continueListing = handler.apply(listing)
    }
  }

  /**
   * Will stop listing the bucket as soon as first result is found.
   */
  @Nullable S3ObjectSummary findObject(
    ListObjectsRequest request,
    Function<S3ObjectSummary, Boolean> matcher)
  {
    S3ObjectSummary result = null
    processAllListObjects(request){
      it.objectSummaries.each{
        if( matcher.apply(it) ){
          result = it
          return ListScan.ABORT
        }
      }
      return ListScan.CONTINUE
    }

    return result
  }

  void deleteObject(String bucketName, String key){
    AmazonS3 s3Client = createS3Client()

    def request = new DeleteObjectRequest(bucketName, key)
    s3Client.deleteObject(request)
  }


}

enum ListScan {
  CONTINUE, ABORT
}
