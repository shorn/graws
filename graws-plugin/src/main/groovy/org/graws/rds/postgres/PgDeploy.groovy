package org.graws.rds.postgres

import org.graws.rds.RdsDeploy
import org.graws.rds.SqlTools

/**
 * This class expectes to be able to connect to pgInstanceUrl,
 * so if you're not running within AWS, you'll need to make sure your bastion
 * tunnel is up.
 */
class PgDeploy {

  RdsDeploy rds

  String pgInstanceUrl
  String masterUserName
  String masterPassword

  /**
   * <ol>
   *   <li>drop dbName</li>
   *   <li>drop dbUser</li>
   *   <li>create dbUser with dbPassword</li>
   *   <li>create dbName</li>
   *   <li>grant all privileges on dbName to dbUser</li>
   * </ol>
   *
   * Will try to terminate all users using the given dbName.
   */
  void recreateDbAndUser(String dbName, String dbUser, String dbPassword){
    println "connecting with $masterUserName to $pgInstanceUrl"
    //noinspection GroovyAssignabilityCheck
    def sql = SqlTools.getSqlConnection(
      pgInstanceUrl, masterUserName, masterPassword)

    println "force close db connections for: $dbName"

    // before doing this, should we revoke the connect privelege for dbUser,
    // so the app/users can't reconnect and make the drop statement fail
    sql.execute "SELECT pg_terminate_backend(pid) FROM pg_stat_activity" +
      " WHERE pg_stat_activity.datname = '$dbName' AND pid <> pg_backend_pid()"

    println "dropping database and user"
    sql.execute "drop database if exists $dbName"
    sql.execute "drop user if exists $dbUser"

    println "creating database and user"
    sql.execute "create user $dbUser with password '$dbPassword'"
    sql.execute "create database $dbName"
    sql.execute "grant all privileges on database $dbName to $dbUser"

    sql.close()
  }

}
