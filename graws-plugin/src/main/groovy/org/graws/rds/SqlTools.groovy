package org.graws.rds

import groovy.sql.GroovyRowResult
import groovy.sql.Sql

import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException

class SqlTools {
  /**
   * autocommit enabled, apparently
   * This thing is pretty horrifying, it exists so that I can log the SQL that I
   * issue, so that it's in the logs on Jenkins.
   * Would be better I think, if we could instead have some logging at the
   * JDBC level, but I didn't want to try and configure up p6spy or similar
   * (postgres driver logging looks way too noisy).
   */
  static Sql getSqlConnection(
    String url,
    String user,
    String password
  ) {
    println "creating connection to $url as $user"
    String driverName = 'org.postgresql.Driver'

    Sql.loadDriver(driverName)
    Connection connection = DriverManager.getConnection(url, user, password)
    return new Sql(connection) {
      @Override
      List<GroovyRowResult> rows(String sql) throws SQLException {
        log(sql)
        return super.rows(sql)
      }

      @Override
      boolean execute(String sql) throws SQLException {
        log(sql)
        return super.execute(sql)
      }

      @Override
      boolean execute(GString gsql) throws SQLException {
        String sql = gsql
        log(sql)
        return super.execute(sql)
      }

      @Override
      boolean execute(String sql, Object[] params) throws SQLException {
        log(sql)
        return super.execute(sql, params)
      }

      void log(String sql) {
        println("SQL: $sql")
      }

      void log(String sql, Object[] params) {
        println("SQL: $sql")
      }
    }
  }

}
