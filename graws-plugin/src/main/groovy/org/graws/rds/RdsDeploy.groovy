package org.graws.rds
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.rds.AmazonRDSClient
import com.amazonaws.services.rds.model.DescribeDBParameterGroupsRequest
import com.amazonaws.services.rds.model.DescribeDBParameterGroupsResult
import org.graws.aws.AwsDeploy

class RdsDeploy {
  RdsClientWrapper rds

  boolean dryRun = false

  boolean reportOptionProblems = true

  AwsDeploy awsDeploy
  String accessKey
  String secretKey

  public RdsClientWrapper getCachedRdsWrapper(){
    if( rds == null ){
      if( dryRun ){
        rds = new RdsClientWrapper(null, true)
      }
      else {
        rds = new RdsClientWrapper(createNewRdsClientForRegion(), false)
      }
    }

    return rds
  }

  public void cleanupCachedRdsClient(){
    rds.shutdown()
    rds = null
  }

  DescribeDBParameterGroupsResult describeParamGroups(
    RdsClientWrapper rds = getCachedRdsWrapper(),
    String groupName)
  {
    return rds.describeParamGroups(groupName)
  }

  public AmazonRDSClient createNewRdsClientForRegion() {
    AmazonRDSClient rdsClient = new AmazonRDSClient(
      awsDeploy.createProviderChain(accessKey, secretKey),
      awsDeploy.clientConfiguration )

    rdsClient.setRegion(Region.getRegion(awsDeploy.defaultRegion))
    return rdsClient
  }




}

class RdsClientWrapper {
  AmazonRDSClient rds

  boolean dryRun

  RdsClientWrapper(AmazonRDSClient rds, boolean dryRun = false) {
    this.rds = rds
    this.dryRun = dryRun
  }

  void shutdown() {
    if( dryRun ){
      println "*** DRY RUN - skipped shutdown()"
      return
    }
    rds.shutdown()
  }

  DescribeDBParameterGroupsResult describeParamGroups(String groupName){
    if( dryRun ){
      println "*** DRY RUN - skipped describeParamGroups"
      return
    }
    def describeRq = new DescribeDBParameterGroupsRequest()
    describeRq.setDBParameterGroupName(groupName)
    return rds.describeDBParameterGroups(describeRq)
  }
}