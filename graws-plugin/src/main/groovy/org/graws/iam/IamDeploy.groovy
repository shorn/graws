package org.graws.iam
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClient
import com.amazonaws.services.identitymanagement.model.*
import org.graws.aws.AwsDeploy

class IamDeploy {
  IamClientWrapper iam
  AwsDeploy aws
  String accessKey
  String secretKey

  boolean dryRun = false

  ServerCertificateMetadata uploadServerCertificate(
    IamClientWrapper iam = getCachedIamWrapper(),
    String srvrCertName,
    String certBody,
    String privateKey,
    String certChain )
  {
    return iam.uploadServerCertificate(
      srvrCertName, certBody, privateKey, certChain).serverCertificateMetadata
  }

  /**
   * Assumes cert with this name exists, will fail if it doesn't.
   */
  GetServerCertificateResult getServerCertificate(
    IamClientWrapper iam = getCachedIamWrapper(),
    String srvrCertName)
  {
    return iam.getServerCertificate(srvrCertName)
  }

  void deleteServerCertificate(
    IamClientWrapper iam = getCachedIamWrapper(),
    String srvrCertName)
  {
    iam.deleteServerCertificate(srvrCertName)
  }

  /**
   *
   * @param srvrCertName exact name, maybe later we could expand to pattern
   * if that's a good idea
   * @return will return null if no cert found.  This method uses the list
   * method to do it's job, so the returned ojbect will be populated as from
   * that.
   */
  ServerCertificateMetadata findServerCertificate(
    IamClientWrapper iam = getCachedIamWrapper(),
    String srvrCertName)
  {
    return listServerCertificates(iam).find{
      it.serverCertificateName == srvrCertName
    }
  }

  List<ServerCertificateMetadata> listServerCertificates(
    IamClientWrapper iam = getCachedIamWrapper() )
  {
    ListServerCertificatesResult certificates = iam.listServerCertificates()
    return certificates.serverCertificateMetadataList
  }

  IamClientWrapper getCachedIamWrapper(){
    if( iam == null ){
      if( dryRun ){
        iam = new IamClientWrapper(null, true)
      }
      else {
        iam = new IamClientWrapper(createNewIamClientForRegion(), false)
      }
    }

    return iam
  }

  AmazonIdentityManagementClient createNewIamClientForRegion() {
    AmazonIdentityManagementClient iamClient =
      new AmazonIdentityManagementClient(
        aws.createProviderChain(accessKey, secretKey),
        aws.clientConfiguration )
    iamClient.setRegion(Region.getRegion(aws.defaultRegion))
    return iamClient
  }



}

class IamClientWrapper {
  AmazonIdentityManagementClient iam
  boolean dryRun = false

  IamClientWrapper(
    AmazonIdentityManagementClient iam,
    boolean dryRun = false)
  {
    this.iam = iam
    this.dryRun = dryRun
  }

  UploadServerCertificateResult uploadServerCertificate(
    String srvrCertName,
    String certBody,
    String privateKey,
    String certChain)
  {
    assert srvrCertName && certBody && privateKey

    if( dryRun ){
      println "*** DRY RUN - skipped upload Server cert for : $srvrCertName"
      return new UploadServerCertificateResult()
    }

    UploadServerCertificateRequest req = new UploadServerCertificateRequest(
      srvrCertName, certBody, privateKey)
    if( certChain ){
      req = req.withCertificateChain(certChain)
    }
    UploadServerCertificateResult certificate = iam.uploadServerCertificate(
      req)
    return certificate
  }

  GetServerCertificateResult getServerCertificate(String srvrCertName){
    assert srvrCertName
    if( dryRun ){
      println "*** DRY RUN - skipped getting Server cert for : $srvrCertName"
      return new GetServerCertificateResult().withServerCertificate(
        new ServerCertificate())
    }

    return iam.getServerCertificate(
      new GetServerCertificateRequest(srvrCertName) )
  }

  ListServerCertificatesResult listServerCertificates(){
    if( dryRun ){
      println "*** DRY RUN - skipped listing Server certificates"
    }

    def req = new ListServerCertificatesRequest().withMaxItems(1000)
    ListServerCertificatesResult certificates = iam.listServerCertificates(req)
    return certificates
  }

  void deleteServerCertificate(String srvrCertName){
    assert srvrCertName
    if( dryRun ){
      println "*** DRY RUN - skipped deleting Server cert for : $srvrCertName"
      return
    }

    DeleteServerCertificateRequest req =
      new DeleteServerCertificateRequest(srvrCertName)
    println "deleting certificate with name ${srvrCertName}..."
    iam.deleteServerCertificate(req)
    println "certificate delete call returned with no errors"
  }


}
