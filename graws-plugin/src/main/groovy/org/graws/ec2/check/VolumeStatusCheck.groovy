package org.graws.ec2.check

import com.amazonaws.services.ec2.model.DescribeVolumesRequest
import com.amazonaws.services.ec2.model.Volume
import org.graws.ec2.Ec2ClientWrapper
import org.graws.util.StatusCheck

class VolumeStatusCheck extends StatusCheck<Volume> {
  // config
  String volumeId
  String expectedStatus

  // dependencies
  Ec2ClientWrapper ec2

  VolumeStatusCheck(
    String volumeId,
    String expectedStatus,
    Ec2ClientWrapper ec2)
  {
    this.volumeId = volumeId
    this.expectedStatus = expectedStatus
    this.ec2 = ec2
  }

  String describe(Volume state){
    assert state
    return "$volumeId is status `$expectedStatus`: ${state.toString()}"
  }

  boolean isReady(Volume state){
    return expectedStatus == state.state
  }

  Volume getStatus(){
    def rq = new DescribeVolumesRequest()
    rq.withVolumeIds(volumeId)
    def statuses = ec2.ec2.describeVolumes(rq)
    assert statuses.volumes.size() == 1
    assert statuses.volumes[0].volumeId == volumeId
    return statuses.volumes[0]
  }

}
