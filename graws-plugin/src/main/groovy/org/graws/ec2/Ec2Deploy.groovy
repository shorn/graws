package org.graws.ec2

import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.ec2.model.CreateSnapshotRequest
import com.amazonaws.services.ec2.model.DescribeVolumeStatusRequest
import com.amazonaws.services.ec2.model.DescribeVolumesRequest
import com.amazonaws.services.ec2.model.Snapshot
import com.amazonaws.services.ec2.model.Volume
import com.amazonaws.services.ec2.model.VolumeStatusInfo
import org.graws.aws.AwsDeploy
import org.graws.ec2.check.VolumeStatusCheck
import org.graws.util.Wait

class Ec2Deploy {
  Ec2ClientWrapper ec2
  AwsDeploy aws
  String accessKey
  String secretKey

  boolean dryRun = false

  // IMPROVE:STO next time workin on this, get rid of the wrapper/dryRun stuff
  Ec2ClientWrapper getCachedEc2Wrapper(){
    if( ec2 == null ){
      if( dryRun ){
        ec2 = new Ec2ClientWrapper(null, true)
      }
      else {
        ec2 = new Ec2ClientWrapper(createNewEc2ClientForRegion(), false)
      }
    }

    return ec2
  }

  AmazonEC2Client createNewEc2ClientForRegion(){
    AmazonEC2Client client =
      new AmazonEC2Client(
        aws.createProviderChain(accessKey, secretKey),
        aws.clientConfiguration)
    client.setRegion(Region.getRegion(aws.defaultRegion))
    return client
  }

  /**
   * This is details about the integrity check status of the volume itself,
   * not its attachment status, etc.
   *
   * @see AmazonEC2Client#describeVolumeStatus(com.amazonaws.services.ec2.model.DescribeVolumeStatusRequest)
   */
  VolumeStatusInfo describeVolumeStatus(
    Ec2ClientWrapper ec2 = getCachedEc2Wrapper(),
    String volumeId)
  {
    assert volumeId
    return ec2.describeVolumeStatus(volumeId)
  }

  Volume describeVolume(
    Ec2ClientWrapper ec2 = getCachedEc2Wrapper(),
    String volumeId)
  {
    assert volumeId
    return ec2.describeVolume(volumeId)
  }

  Snapshot createSnapshot(
    String volumeId,
    String description = "",
    Ec2ClientWrapper ec2 = getCachedEc2Wrapper()
  ) {
    assert volumeId
    return ec2.createSnapshot(volumeId, description)
  }

  boolean waitForVolumeState(
    int waitMinutes = 5,
    int waitIntervalSeconds = 60,
    Ec2ClientWrapper ec2 = getCachedEc2Wrapper(),
    String volumeId,
    String expectedState)
  {
    if( dryRun ){
      return true
    }

    return Wait.until(
      waitMinutes, waitIntervalSeconds,
      new VolumeStatusCheck(volumeId, expectedState, ec2))
  }

}

class Ec2ClientWrapper {
  AmazonEC2Client ec2
  boolean dryRun = false

  Ec2ClientWrapper(
    AmazonEC2Client ec2,
    boolean dryRun = false){
    this.ec2 = ec2
    this.dryRun = dryRun
  }

  Snapshot createSnapshot(String volumeId, String description){
    if( dryRun ){
      println "*** DRY RUN - skipped createSnapshot() call "
      return new Snapshot()
    }

    def rq = new CreateSnapshotRequest()
    rq.dryRunRequest
    rq.withVolumeId(volumeId).withDescription(description)
    def rs = ec2.createSnapshot(rq)
    return rs.snapshot
  }

  VolumeStatusInfo describeVolumeStatus(String volumeId){
    if( dryRun ){
      println "*** DRY RUN - skipped describeVolumeStatus() call "
      return new VolumeStatusInfo()
    }

    def rq = new DescribeVolumeStatusRequest()
    rq.withVolumeIds(volumeId)
    def statuses = ec2.describeVolumeStatus(rq)
    assert statuses.volumeStatuses.size() == 1
    assert statuses.volumeStatuses[0].volumeId == volumeId
    return statuses.volumeStatuses[0].volumeStatus
  }

  Volume describeVolume(String volumeId){
    if( dryRun ){
      println "*** DRY RUN - skipped describeVolumes() call "
      return new Volume()
    }

    def rq = new DescribeVolumesRequest()
    rq.withVolumeIds(volumeId)
    def statuses = ec2.describeVolumes(rq)
    assert statuses.volumes.size() == 1
    assert statuses.volumes[0].volumeId == volumeId
    return statuses.volumes[0]
  }
}
