package org.graws.ecr

import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.ecr.AmazonECRClient
import com.amazonaws.services.ecr.model.AuthorizationData
import com.amazonaws.services.ecr.model.GetAuthorizationTokenRequest
import com.amazonaws.services.ecr.model.GetAuthorizationTokenResult
import org.graws.aws.AwsDeploy

class EcrDeploy {
  EcrClientWrapper ecr
  AwsDeploy aws
  String accessKey
  String secretKey

  boolean dryRun = false


  EcrClientWrapper getCachedEcrWrapper(){
    if( ecr == null ){
      if( dryRun ){
        ecr = new EcrClientWrapper(null, true)
      }
      else {
        ecr = new EcrClientWrapper(createNewEcrClientForRegion(), false)
      }
    }

    return ecr
  }

  AmazonECRClient createNewEcrClientForRegion() {
    AmazonECRClient ecrClient =
      new AmazonECRClient(
        aws.createProviderChain(accessKey, secretKey),
        aws.clientConfiguration )
    ecrClient.setRegion(Region.getRegion(aws.defaultRegion))
    return ecrClient
  }

  /**
   * @param registryName "<aws account id>.dkr.ecr.<aws region>.amazonaws.com"
   * @return the password to use to login to the registry 
   */
  String getAuthorizationToken(
    EcrClientWrapper ecr = getCachedEcrWrapper(), 
    String registryName)
  {
    GetAuthorizationTokenResult authToken = ecr.authorizationToken
    assert authToken.authorizationData.size() == 1
    AuthorizationData authData = authToken.authorizationData[0]
    assert authData.proxyEndpoint == "https://$registryName"
    // This comes back in <user>:<password> format
    String decodedToken = new String(Base64.decoder.decode(
      authData.authorizationToken ))
    // "AWS" is always the username
    assert decodedToken.startsWith("AWS:")
    return decodedToken - "AWS:"
  }
}

class EcrClientWrapper {
  AmazonECRClient ecr
  boolean dryRun = false

  EcrClientWrapper(
    AmazonECRClient ecr,
    boolean dryRun = false)
  {
    this.ecr = ecr
    this.dryRun = dryRun
  }

  GetAuthorizationTokenResult getAuthorizationToken()
  {
    if( dryRun ){
      println "*** DRY RUN - skipped get auth token "
      return new GetAuthorizationTokenResult()
    }

    return ecr.getAuthorizationToken(
      new GetAuthorizationTokenRequest())
  }

}
